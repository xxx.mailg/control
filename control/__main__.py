import sys
import ctypes
import os

from PySide6.QtCore import Qt, QCoreApplication
from PySide6.QtTest import QTest
from PySide6.QtGui import QPixmap
from PySide6.QtWidgets import QApplication, QSplashScreen

from .files import resources_rc  # implicit use
from .structs.db import DB
from .ui.mainwindow import MainWindow


def main():
    if os.name == 'nt':
        myappid = 'UGRES.control.2.0'  # arbitrary string
        ctypes.windll.shell32.SetCurrentProcessExplicitAppUserModelID(myappid)

    app = QApplication(sys.argv)

    QCoreApplication.setOrganizationName("UGRES")
    QCoreApplication.setApplicationName("Control")

    splash = QSplashScreen(QPixmap(":/images/logo.jpg"))
    splash.show()

    splash.showMessage("Подключение к БД",
                       Qt.AlignmentFlag.AlignBottom | Qt.AlignmentFlag.AlignHCenter)

    splash.showMessage("" if DB().is_valid else DB().error,
                       Qt.AlignmentFlag.AlignBottom | Qt.AlignmentFlag.AlignHCenter)

    if not DB.is_valid:
        QTest.qWait(10000)
        sys.exit(0)

    widget = MainWindow()
    splash.finish(widget)
    widget.show()
    exit_code = app.exec()

    DB.close()
    return exit_code


if __name__ == "__main__":
    exit_code = main()
    print('Closed')
    sys.exit(exit_code)
