# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'issue_add_tail.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractItemView, QApplication, QDateTimeEdit, QHBoxLayout,
    QHeaderView, QLabel, QPushButton, QSizePolicy,
    QSpacerItem, QTableView, QVBoxLayout, QWidget)

class Ui_IssueAddTail(object):
    def setupUi(self, IssueAddTail):
        if not IssueAddTail.objectName():
            IssueAddTail.setObjectName(u"IssueAddTail")
        IssueAddTail.resize(373, 236)
        self.main = QWidget(IssueAddTail)
        self.main.setObjectName(u"main")
        self.main.setGeometry(QRect(9, 9, 359, 212))
        self.verticalLayout = QVBoxLayout(self.main)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.responsible = QTableView(self.main)
        self.responsible.setObjectName(u"responsible")
        self.responsible.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.responsible.setAlternatingRowColors(True)
        self.responsible.setSelectionMode(QAbstractItemView.SingleSelection)
        self.responsible.horizontalHeader().setVisible(False)
        self.responsible.horizontalHeader().setStretchLastSection(True)

        self.horizontalLayout_2.addWidget(self.responsible)

        self.verticalLayout_4 = QVBoxLayout()
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalSpacer = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_4.addItem(self.verticalSpacer)

        self.add_resp_all = QPushButton(self.main)
        self.add_resp_all.setObjectName(u"add_resp_all")

        self.verticalLayout_4.addWidget(self.add_resp_all)

        self.add_resp = QPushButton(self.main)
        self.add_resp.setObjectName(u"add_resp")
        self.add_resp.setStyleSheet(u"QPushButton::menu-indicator{width:0px;}")

        self.verticalLayout_4.addWidget(self.add_resp)

        self.rem_resp = QPushButton(self.main)
        self.rem_resp.setObjectName(u"rem_resp")

        self.verticalLayout_4.addWidget(self.rem_resp)

        self.verticalSpacer_2 = QSpacerItem(20, 40, QSizePolicy.Minimum, QSizePolicy.Expanding)

        self.verticalLayout_4.addItem(self.verticalSpacer_2)


        self.horizontalLayout_2.addLayout(self.verticalLayout_4)


        self.verticalLayout.addLayout(self.horizontalLayout_2)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label_5 = QLabel(self.main)
        self.label_5.setObjectName(u"label_5")
        sizePolicy = QSizePolicy(QSizePolicy.Preferred, QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.label_5.sizePolicy().hasHeightForWidth())
        self.label_5.setSizePolicy(sizePolicy)
        self.label_5.setMinimumSize(QSize(100, 0))

        self.horizontalLayout.addWidget(self.label_5)

        self.repair_before = QDateTimeEdit(self.main)
        self.repair_before.setObjectName(u"repair_before")

        self.horizontalLayout.addWidget(self.repair_before)


        self.verticalLayout.addLayout(self.horizontalLayout)


        self.retranslateUi(IssueAddTail)

        QMetaObject.connectSlotsByName(IssueAddTail)
    # setupUi

    def retranslateUi(self, IssueAddTail):
        IssueAddTail.setWindowTitle(QCoreApplication.translate("IssueAddTail", u"Form", None))
        self.add_resp_all.setText(QCoreApplication.translate("IssueAddTail", u"\u0412\u0441\u0435", None))
        self.add_resp.setText(QCoreApplication.translate("IssueAddTail", u"+", None))
        self.rem_resp.setText(QCoreApplication.translate("IssueAddTail", u"-", None))
        self.label_5.setText(QCoreApplication.translate("IssueAddTail", u"\u0423\u0441\u0442\u0440\u0430\u043d\u0438\u0442\u044c \u0434\u043e", None))
    # retranslateUi

