# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'issue_edit_tail.ui'
##
## Created by: Qt User Interface Compiler version 6.6.1
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QDateTimeEdit, QHBoxLayout,
    QLabel, QSizePolicy, QWidget)

class Ui_IssueEditTail(object):
    def setupUi(self, IssueEditTail):
        if not IssueEditTail.objectName():
            IssueEditTail.setObjectName(u"IssueEditTail")
        IssueEditTail.resize(433, 66)
        self.main = QWidget(IssueEditTail)
        self.main.setObjectName(u"main")
        self.main.setGeometry(QRect(9, 9, 410, 40))
        self.horizontalLayout = QHBoxLayout(self.main)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label_2 = QLabel(self.main)
        self.label_2.setObjectName(u"label_2")
        self.label_2.setMinimumSize(QSize(100, 0))
        self.label_2.setMaximumSize(QSize(100, 16777215))

        self.horizontalLayout.addWidget(self.label_2)

        self.responsible = QComboBox(self.main)
        self.responsible.setObjectName(u"responsible")

        self.horizontalLayout.addWidget(self.responsible)

        self.label_5 = QLabel(self.main)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setMinimumSize(QSize(100, 0))
        self.label_5.setMaximumSize(QSize(100, 16777215))

        self.horizontalLayout.addWidget(self.label_5)

        self.repair_before = QDateTimeEdit(self.main)
        self.repair_before.setObjectName(u"repair_before")

        self.horizontalLayout.addWidget(self.repair_before)


        self.retranslateUi(IssueEditTail)

        QMetaObject.connectSlotsByName(IssueEditTail)
    # setupUi

    def retranslateUi(self, IssueEditTail):
        IssueEditTail.setWindowTitle(QCoreApplication.translate("IssueEditTail", u"Form", None))
        self.label_2.setText(QCoreApplication.translate("IssueEditTail", u"\u0426\u0435\u0445", None))
        self.label_5.setText(QCoreApplication.translate("IssueEditTail", u"\u0423\u0441\u0442\u0440\u0430\u043d\u0438\u0442\u044c \u0434\u043e", None))
    # retranslateUi

