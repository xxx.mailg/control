# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'issue_fix.ui'
##
## Created by: Qt User Interface Compiler version 6.5.2
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QAbstractItemView, QApplication, QDialog, QGroupBox,
    QHBoxLayout, QListView, QPlainTextEdit, QPushButton,
    QSizePolicy, QSpacerItem, QVBoxLayout, QWidget)

class Ui_fix(object):
    def setupUi(self, fix):
        if not fix.objectName():
            fix.setObjectName(u"fix")
        fix.resize(711, 439)
        self.verticalLayout_3 = QVBoxLayout(fix)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.groupBox = QGroupBox(fix)
        self.groupBox.setObjectName(u"groupBox")
        self.verticalLayout = QVBoxLayout(self.groupBox)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.pte_text = QPlainTextEdit(self.groupBox)
        self.pte_text.setObjectName(u"pte_text")

        self.verticalLayout.addWidget(self.pte_text)


        self.verticalLayout_3.addWidget(self.groupBox)

        self.groupBox_2 = QGroupBox(fix)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.verticalLayout_2 = QVBoxLayout(self.groupBox_2)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.lvFiles = QListView(self.groupBox_2)
        self.lvFiles.setObjectName(u"lvFiles")
        self.lvFiles.setEditTriggers(QAbstractItemView.NoEditTriggers)
        self.lvFiles.setAlternatingRowColors(True)
        self.lvFiles.setSelectionMode(QAbstractItemView.ExtendedSelection)

        self.verticalLayout_2.addWidget(self.lvFiles)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.pb_add = QPushButton(self.groupBox_2)
        self.pb_add.setObjectName(u"pb_add")

        self.horizontalLayout.addWidget(self.pb_add)

        self.pb_delete = QPushButton(self.groupBox_2)
        self.pb_delete.setObjectName(u"pb_delete")

        self.horizontalLayout.addWidget(self.pb_delete)

        self.horizontalSpacer = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout.addItem(self.horizontalSpacer)

        self.pb_accept = QPushButton(self.groupBox_2)
        self.pb_accept.setObjectName(u"pb_accept")

        self.horizontalLayout.addWidget(self.pb_accept)

        self.pb_cancel = QPushButton(self.groupBox_2)
        self.pb_cancel.setObjectName(u"pb_cancel")

        self.horizontalLayout.addWidget(self.pb_cancel)


        self.verticalLayout_2.addLayout(self.horizontalLayout)


        self.verticalLayout_3.addWidget(self.groupBox_2)


        self.retranslateUi(fix)

        QMetaObject.connectSlotsByName(fix)
    # setupUi

    def retranslateUi(self, fix):
        fix.setWindowTitle(QCoreApplication.translate("fix", u"\u041e\u0442\u043c\u0435\u0442\u043a\u0430 \u043e\u0431 \u0443\u0441\u0442\u0440\u0430\u043d\u0435\u043d\u0438\u0438", None))
        self.groupBox.setTitle(QCoreApplication.translate("fix", u"\u0422\u0435\u043a\u0441\u0442", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("fix", u"\u0424\u0430\u0439\u043b\u044b", None))
        self.pb_add.setText(QCoreApplication.translate("fix", u"\u0414\u043e\u0431\u0430\u0432\u0438\u0442\u044c", None))
        self.pb_delete.setText(QCoreApplication.translate("fix", u"\u0423\u0434\u0430\u043b\u0438\u0442\u044c", None))
        self.pb_accept.setText(QCoreApplication.translate("fix", u"\u041e\u041a", None))
        self.pb_cancel.setText(QCoreApplication.translate("fix", u"\u041e\u0442\u043c\u0435\u043d\u0430", None))
    # retranslateUi

