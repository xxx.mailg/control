import os

from PySide6.QtCore import *
from PySide6.QtWidgets import *
from PySide6.QtGui import *

from ..structs.user import User
from ..structs.db import DB
from .design.issue_fix_ui import Ui_fix


class IssueFixWin(QDialog):
    def __init__(self, id, parent=None):
        super().__init__(parent)
        assert User.is_valid
        self.ui = Ui_fix()
        self.ui.setupUi(self)
        self.files_dict = {}
        self.id = id

        self.ui.pb_accept.clicked.connect(self.accept)
        self.ui.pb_cancel.clicked.connect(self.reject)

        self.ui.pb_add.clicked.connect(self.add_file)
        self.ui.pb_delete.clicked.connect(self.delete_file)

        self.files_list = QStringListModel(self)
        self.ui.lvFiles.setModel(self.files_list)
        self._populate_model()

    def accept(self):
        cursor = DB.connection().cursor(prepared=True)
        query = """
                INSERT INTO files (issue_id, filename, data)
                VALUES (%s, %s, %s);
                """
        for key, value in self.files_dict.items():
            params = (self.id, key, value)
            cursor.execute(query, params)

        query = "UPDATE issue SET repair_date = now(), repair_department = %s, repair_user= %s, repair_comment = %s WHERE id = %s"
        cursor.execute(query, (User.department, User.short_name(), self.ui.pte_text.toPlainText(), self.id))

        DB.connection().commit()
        cursor.close()

        return super().accept()

    def reject(self):
        return super().reject()

    def add_file(self):
        files = QFileDialog.getOpenFileNames(self, "Выбрать файл")
        if len(files[0]) == 0:
            return
        for file in files[0]:
            with open(file, 'rb') as f:
                file_data = f.read()
                file_name = os.path.basename(f.name)
                self.files_dict[file_name] = file_data
                self._populate_model()

    def _populate_model(self):
        files = sorted(self.files_dict.keys())
        self.files_list.setStringList(files)

    def delete_file(self):
        selection = self.ui.lvFiles.selectionModel()
        if selection.hasSelection():
            for item in selection.selectedIndexes():
                self.files_dict.pop(item.data())
            self._populate_model()
