from functools import partial

from PySide6.QtCore import *
from PySide6.QtWidgets import *
from PySide6.QtGui import *

from control.structs.db import DB
from control.structs.issue import Issue
from control.structs.simple_model import SimpleModel

from .design.issue_add_ui import Ui_IssueAdd
from .design.issue_add_tail_ui import Ui_IssueAddTail
from .design.issue_edit_tail_ui import Ui_IssueEditTail


class IssueBaseWin(QDialog):
    issues = Signal(object)

    def __init__(self, parent=None):
        super().__init__(parent)

        self.ui = Ui_IssueAdd()
        self.ui.setupUi(self)

        self.ui.category.setModel(SimpleModel("category", self))
        self.ui.subcategory.setModel(SimpleModel("subcategory", self))

        self.ui.accept.clicked.connect(self.accept)
        self.ui.cancel.clicked.connect(self.reject)


class IssueAddWin(IssueBaseWin):
    def __init__(self, parent=None):
        super().__init__(parent)

        self.tail = Ui_IssueAddTail()
        self.tail.setupUi(self.ui.gbresponsible)

        self.ui.tail_placeholder.addWidget(self.tail.main)

        self.responsible_model = QStringListModel(self)
        self.tail.responsible.setModel(self.responsible_model)

        menu = QMenu(self.tail.add_resp)
        self.tail.add_resp.setMenu(menu)

        # menu responsible
        cursor = DB.connection().cursor()
        query = """SELECT name FROM department WHERE responsible is TRUE ORDER BY name"""
        cursor.execute(query)

        for row in cursor.fetchall():
            menu.addAction(row[0], partial(self.add_responsible, row[0]))

        self.tail.add_resp_all.clicked.connect(lambda: [self.add_responsible(i.text()) for i in menu.actions()])
        self.tail.rem_resp.clicked.connect(self.rem_responsible)

    def add_responsible(self, name: str):
        list_resp = self.responsible_model.stringList()
        if name not in list_resp:
            list_resp.append(name)
            self.responsible_model.setStringList(sorted(list_resp))

    def rem_responsible(self):
        if self.tail.responsible.selectionModel().hasSelection():
            name = self.tail.responsible.selectionModel().currentIndex().data()

            list_resp = self.responsible_model.stringList()
            list_resp.remove(name)
            self.responsible_model.setStringList(sorted(list_resp))

    def accept(self) -> None:
        issues: list[Issue] = []
        for responsible in self.responsible_model.stringList():
            issue = Issue()
            issue.place = self.ui.place.text()
            issue.issue = self.ui.issue.toPlainText()
            issue.category = self.ui.category.currentText()
            issue.subcategory = self.ui.subcategory.currentText()
            issue.measures = self.ui.measures.toPlainText()
            issue.responsible = responsible
            issue.repair_before = self.tail.repair_before.dateTime()
            issues.append(issue)

        self.issues.emit(issues)
        return super().accept()


class IssueEditWin(IssueBaseWin):
    def __init__(self, issue: Issue, parent=None):
        super().__init__(parent)

        self.tail = Ui_IssueEditTail()
        self.tail.setupUi(self.ui.gbresponsible)
        self.ui.tail_placeholder.addWidget(self.tail.main)

        # menu responsible
        cursor = DB.connection().cursor()
        query = """SELECT name FROM department WHERE responsible is TRUE ORDER BY name"""
        cursor.execute(query)

        for row in cursor.fetchall():
            self.tail.responsible.addItem(row[0])

        self.ui.has_issue.setChecked(issue.issue != "")

        self.ui.place.setText(issue.place)
        self.ui.issue.setPlainText(issue.issue)
        self.ui.category.setCurrentText(issue.category)
        self.ui.subcategory.setCurrentText(issue.subcategory)
        self.ui.measures.setPlainText(issue.measures)

        self.tail.responsible.setCurrentText(issue.responsible)
        self.tail.repair_before.setDateTime(issue.repair_before)
        self.issue = issue

    def accept(self) -> None:
        self.issue.place = self.ui.place.text()
        self.issue.issue = self.ui.issue.toPlainText()
        self.issue.category = self.ui.category.currentText()
        self.issue.subcategory = self.ui.subcategory.currentText()
        self.issue.measures = self.ui.measures.toPlainText()
        self.issue.responsible = self.tail.responsible.currentText()
        self.issue.repair_before = self.tail.repair_before.dateTime()

        return super().accept()
