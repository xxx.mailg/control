import re
from openpyxl import load_workbook
from PySide6.QtCore import *
from PySide6.QtWidgets import *
from PySide6.QtGui import *

from control.structs.db import DB
from control.structs.issue import Issue
from control.structs.issues_table_view import IssueTableView
from control.structs.simple_model import SimpleModel
from control.structs.user import User

from ..structs.inspection import Inspection
from .design.inspection_ui import Ui_Inspection
from .issue_add_win import IssueEditWin, IssueAddWin


class InspectionWin(QDialog):
    """Window with all inforamtion about inspection"""

    def __init__(self, id: int = 0, parent=None):
        super().__init__(parent)

        assert type(id) == int

        if not User.is_valid:  # TODO: remove later
            QMessageBox.warning(None, "Ошибка авторизации",
                                "Вы не авторизовались.\nАвторизуйтесь и попробуйте еще раз: admin/admin")
        assert User.is_valid

        self.ui = Ui_Inspection()
        self.ui.setupUi(self)

        self.ui.inspection_type.setModel(SimpleModel("type", self))

        self.settings = QSettings(self)
        self.inspection = Inspection(id, self)
        self.issues = IssueTableView(self)
        self.issues.change_model(self.inspection.issues_model)

        self.ui.inspection_id.setText(self.inspection.work_id)
        self.ui.inspection_date.setText(self.inspection.date_str())
        self.ui.level.setText(str(self.inspection.level))
        self.ui.inspection_type.setCurrentText(self.inspection.type)
        self.ui.department.setText(self.inspection.department)
        self.ui.inspector.setText(self.inspection.inspector)
        self.ui.shift.setCurrentText(str(self.inspection.shift))
        self.ui.issues_layout.insertWidget(0, self.issues)

        self.ui.ok.clicked.connect(self.accept)
        self.ui.cancel.clicked.connect(self.reject)

        self.ui.add_issue_button.clicked.connect(self.add_issue_win)

        self.issues.doubleClicked.connect(self._double_clicked)

        self.ui.import_button.clicked.connect(self.import_inspection)

        self.ui.level.setText(str(User.level))
        self.ui.inspection_type.setEnabled(User.level == 3)
        self.ui.import_button.setVisible(User.department == "ООТиПБ")

    # добавить проверку
    def accept(self):
        if self.inspection.id != 0:
            self.update()
        else:
            self.add_new()
        return super().accept()

    def update(self):
        cursor = DB.connection().cursor(prepared=True)
        for issue in self.inspection.issues_model:
            query = "UPDATE issue SET `place` = %s, `issue` = %s, `repair_before` = %s, `responsible` = %s, `category` = %s, `subcategory` = %s, `measures` = %s WHERE id = %s"
            params = (issue.place,
                      issue.issue,
                      issue.repair_before.toString(Qt.DateFormat.ISODate),
                      issue.responsible,
                      issue.category,
                      issue.subcategory,
                      issue.measures,
                      issue.id)
            cursor.execute(query, params)
        DB.connection().commit()
        cursor.close()

    def add_new(self):
        # TODO: add try catch to show error messages
        cursor = DB.connection().cursor(prepared=True)
        query = "INSERT INTO `inspection` (`work_id`, `level`, `type`, `department`, `inspector`, `shift`) VALUES (%s, %s, %s, %s, %s, %s);"
        params = (self.ui.inspection_id.text(),
                  self.ui.level.text(),
                  self.ui.inspection_type.currentText(),
                  self.ui.department.text(),
                  self.ui.inspector.text(),
                  self.ui.shift.currentText())
        cursor.execute(query, params)
        DB.connection().commit()

        inspection_id = cursor.lastrowid

        for issue in self.issues.model().sourceModel():
            query = """INSERT INTO `issue` (`inspection_id`, `place`, `issue`, `repair_before`, `responsible`, `category`, `subcategory`, `measures`)
                        VALUES (%s, %s, %s, %s, %s, %s, %s, %s);"""
            params = (inspection_id,
                      issue.place,
                      issue.issue,
                      issue.repair_before.toString(Qt.DateFormat.ISODate),
                      issue.responsible,
                      issue.category,
                      issue.subcategory,
                      issue.measures)
            cursor.execute(query, params)
        DB.connection().commit()
        cursor.close()

    def import_inspection(self):
        if not re.search(r"Предписание|Аудит|Акт ДОТ", self.ui.inspection_type.currentText()):
            QMessageBox.information(None, "Информация",
                                    f"Импорт '{self.ui.inspection_type.currentText()}' в данный момент не поддерживается")
            return

        file_path, _ = QFileDialog.getOpenFileName(
            None, "Загрузить из файла", self.settings.value("import/last", "C:\\"),
            "Excel (*.xlsx *.xls)")
        if not file_path:
            return
        self.settings.setValue("import/last", file_path)

        workbook = load_workbook(file_path, data_only=True)
        sheet = workbook.active

        # Определение позиции данных. Меняется в зависимости от типа документа
        department_pos = (34, 3)
        inspection_type = self.ui.inspection_type.currentText()

        if inspection_type == "Предписание":
            department_pos = (8, 6) if sheet.cell(row=8, column=6).value else (15, 2)
        elif inspection_type == "Акт ДОТ":
            department_pos = (8, 4)

        department = sheet.cell(row=department_pos[0], column=department_pos[1]).value

        # Считывание данных из Excel
        row = 38  # начальная позиция с таблицей замечаний
        issues: list[Issue] = []

        while True:
            # Проверка на конец данных
            issue_seq_number = sheet.cell(row=row, column=1).value
            issue = sheet.cell(row=row, column=2).value

            if not issue_seq_number or not issue:
                break

            # поле с датой может содержать текст
            date_str = str(sheet.cell(row=row, column=4).value).split()[0]
            date = QDate.fromString(date_str, "dd.MM.yyyy")
            issue = Issue(0)

            issue.place = department
            issue.issue = issue
            issue.repair_before = date
            issue.responsible = department
            issue.category = sheet.cell(row=row, column=6).value
            issue.subcategory = sheet.cell(row=row, column=7).value
            issue.measures = sheet.cell(row=row, column=3).value

            issues.append(issue)
            row += 1
        workbook.close()

        self.add_issues(issues)

    def add_issue_win(self):
        win = IssueAddWin(self)
        win.issues.connect(self.add_issues)
        win.exec()

    def add_issues(self, issues: list[Issue]):
        for issue in issues:
            row = self.inspection.issues_model.rowCount()
            self.inspection.issues_model.insertRow(row, QModelIndex())

            index = self.inspection.issues_model.index(row, 0)
            self.inspection.issues_model.setData(index, issue, Qt.ItemDataRole.UserRole)

    def _double_clicked(self, index: QModelIndex):
        issue = self.inspection.issues_model.get_issue(index.row())
        win = IssueEditWin(issue, self)
        win.exec()
