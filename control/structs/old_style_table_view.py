from pathlib import Path
from PySide6.QtWidgets import *
from PySide6.QtCore import *
from PySide6.QtGui import *

from ..structs.db import DB

from ..structs.user import Access, User
from ..ui.inspection_win import InspectionWin
from .base_table_view import BaseTableView
from .old_style_model import OldStyleModel
from ..ui.issue_fix_win import IssueFixWin


class OldStuleTableView(BaseTableView):
    request_update = Signal()

    def __init__(self, parent: QWidget) -> None:
        super().__init__(parent)
        self._model = OldStyleModel(self)
        self._proxy_model.setSourceModel(self._model)
        self.setContextMenuPolicy(Qt.ContextMenuPolicy.CustomContextMenu)
        self.customContextMenuRequested.connect(self.context_menu)
        self.dir = QTemporaryDir()

    def fetch(self, begin_date: QDate, end_date: QDate):
        self.setDisabled(True)
        if self._model.fetch(begin_date, end_date):
            self.setEnabled(True)

    def _double_clicked(self, index: QModelIndex):
        id = self._model.get_id(index)
        if InspectionWin(id=id, parent=self).exec():
            self.request_update.emit()

    def context_menu(self, position):
        selected = self.selectionModel()
        if not selected.hasSelection():
            return

        menu = QMenu(self)

        id = selected.selectedIndexes()[0].data()
        cursor = DB.connection().cursor(prepared=True)
        query = "SELECT filename FROM files WHERE issue_id = %s ORDER BY filename;"
        cursor.execute(query, (id,))
        rows: list[list] = cursor.fetchall()

        if rows:
            files = menu.addMenu('Файлы')
            for row in rows:
                files.addAction(row[0], self.open_file)

        cursor.close()

        if User.access(Access.FIX) and not selected.selectedIndexes()[15].data() and selected.selectedIndexes()[10].data() == User.department:  # если свой цех
            menu.addAction('Устранить', self.show_issue_fix_win)

        level = selected.selectedIndexes()[3].data()
        
        if User.access(Access.CONFIRM) and selected.selectedIndexes()[16].data() and (level != '3' or level == User.level) and not selected.selectedIndexes()[18].data(): #ступень не 3, ступень 3 и юзер 3 ступени
            menu.addAction('Подтвердить', self.confirm_issue)
            menu.addAction('Отклонить', self.clear_files)

        menu.addAction(QIcon(":/icons/off_excel.png"), 'Экспорт таблицы в Excel', lambda: print(3))

        position = self.viewport().mapToGlobal(position)
        menu.popup(position)

    # --------------- show issue_fix --------------------

    def show_issue_fix_win(self):
        selected = self.selectionModel()
        if not selected.hasSelection():
            return

        id = selected.selectedIndexes()[0].data()

        if (IssueFixWin(id, self).exec()):
            self.request_update.emit()

    def clear_files(self):
        selected = self.selectionModel()
        if not selected.hasSelection():
            return

        id = selected.selectedIndexes()[0].data()

        cursor = DB.connection().cursor(prepared=True)
        query = "DELETE FROM files WHERE issue_id = %s"
        cursor.execute(query, (id,))
        query = "UPDATE issue SET  repair_date = NULL, repair_department = NULL, repair_user= NULL, repair_comment = NULL WHERE id = %s"
        cursor.execute(query, (id,))
        DB.connection().commit()
        cursor.close()
        self.request_update.emit()

    def confirm_issue(self):
        selected = self.selectionModel()
        if not selected.hasSelection():
            return

        id = selected.selectedIndexes()[0].data()

        cursor = DB.connection().cursor(prepared=True)
        query = """UPDATE issue
                SET confirmation_date = now(), confirmation_department = %s, confirmation_user= %s, confirmation_comment = 'Подтверждено'
                WHERE id = %s
                """
        cursor.execute(query, (User.department, User.short_name(), id))
        DB.connection().commit()
        cursor.close()
        self.request_update.emit()

    def open_file(self):
        action: QAction = self.sender()
        filename = action.text()

        selected = self.selectionModel()
        assert selected.hasSelection()

        id = selected.selectedIndexes()[0].data()
        print(action.text())

        cursor = DB.connection().cursor(prepared=True)
        query = "SELECT data FROM files WHERE issue_id = %s AND filename = %s"
        cursor.execute(query, (id, filename))
        row: list = cursor.fetchone()

        if self.dir.isValid():
            filepath = Path(self.dir.path()) / filename
            print(filepath)
            with open(filepath, 'wb') as f:
                f.write(row[0])
            cursor.close()

            QDesktopServices.openUrl(QUrl.fromLocalFile(filepath))
