from PySide6.QtCore import *
from PySide6.QtWidgets import *
from PySide6.QtGui import *
from mysql.connector import PoolError
from threading import Thread
from threading import Thread, Event

from .db import DB


class LoaderThread(Thread):
    """Loader thread. Can be stopped at any time to break loading"""

    def __init__(self):
        super().__init__()
        self._stop_event = Event()
        self.current = 0
        self.total = 0
        self.status = False

        self.params = ()
        self.query = ""

    def exec(self, data, description):
        raise NotImplemented

    def stop(self):
        self._stop_event.set()

    def run(self):
        assert (self.query != "")

        try:
            cursor = DB.connection().cursor(prepared=True)
        except PoolError:
            print("Ошибка", "Превышен лимит запросов")
            return

        cursor.execute(self.query, self.params)
        data: list = cursor.fetchall()
        description = cursor.description
        self.total = len(data)
        self.exec(data, description)

        DB.close_current()
        self.status = True
