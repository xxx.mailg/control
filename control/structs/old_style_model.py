from datetime import datetime
from typing import Any
from PySide6.QtCore import *
from PySide6.QtWidgets import *
from PySide6.QtGui import *

from .db import DB
from .base_table_model import BaseTableModel
from .loader_thread_base import LoaderThread
from .user import User


class LoaderThreadIssue2(LoaderThread):
    """Loader for inspections. Can be stopped at any time to break loading"""

    def __init__(self, begin_date: QDate, end_date: QDate):
        super().__init__()
        begin = begin_date.toString(Qt.DateFormat.ISODate)
        end = end_date.addDays(1).toString(Qt.DateFormat.ISODate)

        self.issues2 = []
        self.params = (begin, end)
        self.query = """
            SELECT
                iss.id,
                ins.work_id,
                ins.created,
                ins.level,
                ins.type,
                ins.department,
                ins.inspector,
                ins.shift,
                iss.place,
                iss.issue,
                iss.responsible,
                iss.repair_before,
                iss.category,
                iss.subcategory,
                iss.measures,
                iss.repair_date,
                CONCAT(iss.repair_department, ' ', iss.repair_user) AS fixed_info,
                iss.repair_comment,
                iss.confirmation_date,
                CONCAT(iss.confirmation_department, ' ', iss.confirmation_user) AS confirmed_info,
                iss.confirmation_comment,
                ins.id
            FROM inspection AS ins
            JOIN issue AS iss ON ins.id = iss.inspection_id
            WHERE ins.created BETWEEN %s AND %s
            ORDER BY iss.id;
        """

    def exec(self, data, description):
        for row in data:
            self.current += 1
            issue2 = []
            for index, _ in enumerate(description):
                if isinstance(row[index], datetime):
                    issue2.append(QDateTime(row[index]))
                else:
                    issue2.append(row[index])
            self.issues2.append(issue2)


class OldStyleModel(BaseTableModel):
    def __init__(self, parent: QObject) -> None:
        super().__init__(parent)
        self.job = None
        self._headers = (
            "№",
            "Вн №",
            "Дата проверки",
            "Ступень",
            "Тип",
            "Цех",
            "Проверяющий",
            "Смена",
            "Объект",
            "Замечание",
            "Ответственный",
            "Устранить до",
            "Категория",
            "Подкатегория",
            "Мероприятия",
            "Дата устр",
            "Устранивший",
            "Комментарий",
            "Дата подтв",
            "Подтвердивший",
            "Комментарий",
        )

    def fetch(self, begin_date: QDate, end_date: QDate):
        if not DB.is_available():
            QMessageBox.critical(None, "Ошибка", "Превышен лимит запрсов. Дождитесь завршения предыдущего запроса.")
            return False

        self.clear()
        self._data = [[""] * len(self._headers)]

        if self.job and self.job.is_alive():
            self.job.stop()

        self.job = LoaderThreadIssue2(begin_date, end_date)
        self.job.start()

        while self.job.is_alive():
            self.beginResetModel()
            self._data[0][0] = "Загрузка"
            if self.job.current:
                self._data[0][1] = f"{self.job.current}"
                self._data[0][2] = f"из"
                self._data[0][3] = f"{self.job.total}"
            self.endResetModel()

            self.job.join(0.01)
            QCoreApplication.processEvents()

        self.beginResetModel()
        self._data = self.job.issues2
        self.endResetModel()
        return self.job.status

    def get_id(self, index: QModelIndex | int) -> int:
        return self._data[index.row()][self.columnCount()]

    def data(self, index: QModelIndex | QPersistentModelIndex, role: int = ...) -> Any:
        if User.is_valid and role == Qt.BackgroundRole:
            department = index.siblingAtColumn(10).data(Qt.ItemDataRole.DisplayRole)

            fix_before = index.siblingAtColumn(11).data(Qt.ItemDataRole.DisplayRole)
            if not fix_before or not isinstance(fix_before, QDateTime):
                return super().data(index, role)
            assert isinstance(fix_before, QDateTime)
            
            confirmation_date = index.siblingAtColumn(18).data(Qt.ItemDataRole.DisplayRole)
            if isinstance(confirmation_date, str):
                return super().data(index, role)
            
            if User.department == department and confirmation_date:
                return QColor(128, 255, 128)
            
            if User.department == department and fix_before < QDateTime.currentDateTime():
                return QColor(255, 128, 128)
            
            if User.department == department and fix_before < QDateTime.currentDateTime().addDays(7):
                return QColor(255, 180, 128)

            if User.department == department and fix_before > QDateTime.currentDateTime():
                return QColor(255, 255, 128)
                              
        return super().data(index, role)
